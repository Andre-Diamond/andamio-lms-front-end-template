import {
  Badge,
  Box,
  Button,
  Container,
  Divider,
  Flex,
  Grid,
  GridItem,
  Heading,
  ListItem,
  OrderedList,
  Spacer,
  Stack,
  StackDivider,
  Text,
} from "@chakra-ui/react";
import Link from "next/link";
import * as React from "react";

import SLT from "@/src/components/ui/Text/SLT";
import GetHelp from "../Course/GetHelp";
import LessonNavigation from "./LessonNavigation";

// Props
// SLT
// children
// Next Lesson?

type Props = {
  children?: React.ReactNode;
  moduleNumber: number;
  sltId: string;
  slug: string;
  title: string;
  nextModule?: string;
};

const ProjectLayout: React.FC<Props> = ({ children, moduleNumber, sltId, slug, title, nextModule }) => {
  return (
    <>
      <Box w="95%" marginTop="2em">
        <Badge>Mini-Project | Module {moduleNumber}</Badge>
        <Text fontSize="4xl" fontWeight="900" py="2">
          {title}
        </Text>
        <Divider />
        <SLT moduleNumber={parseInt(sltId.substring(0, 3))} id={sltId} />
        <Box borderLeft="1px" borderColor="theme.green">{children}</Box>

        <LessonNavigation moduleNumber={moduleNumber} currentSlug={slug} />

        {nextModule && (
          <Flex direction="row">
            <Spacer />
            <Link href={`/modules/${nextModule}/slts`}>
              <Button my="1em">Proceed to Module {nextModule}</Button>
            </Link>
          </Flex>
        )}
      </Box>
      <Divider py="5" w="90%" marginLeft="1em" />
      <Box mt="10">
        <GetHelp />
      </Box>
    </>
  );
};

export default ProjectLayout;
