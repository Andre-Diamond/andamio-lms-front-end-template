import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./../module.json";
import Docs from "./Docs.mdx";


export default function Lesson() {
  const slug = "9992";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={999} sltId="999.2" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs />
    </LessonLayout>
  );
}